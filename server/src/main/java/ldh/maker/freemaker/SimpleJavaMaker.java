package ldh.maker.freemaker;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ldh on 2017/4/8.
 */
public class SimpleJavaMaker extends FreeMarkerMaker<SimpleJavaMaker> {

    protected String projectRootPackage;
    protected String author;
    protected String description;

    public SimpleJavaMaker projectRootPackage(String projectRootPackage) {
        this.projectRootPackage = projectRootPackage;
        return this;
    }

    public SimpleJavaMaker auhtor(String author) {
        this.author = author;
        return this;
    }

    public SimpleJavaMaker description(String description) {
        this.description = description;
        return this;
    }

    public SimpleJavaMaker data(String key, Object value) {
        data.put(key, value);
        return this;
    }

    @Override
    public SimpleJavaMaker make() {
        data();
        this.out(ftl, data);
        return this;
    }

    @Override
    public void data() {
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String str=sdf.format(new Date());
        data.put("Author",author);
        data.put("DATE", str);
        data.put("Description", description);
        data.put("projectRootPackage", projectRootPackage);
    }
}
