class ${util.firstUpper(table.javaName)} {
  ${util.firstUpper(table.javaName)}({${util.dartBeanParams(table)}});

  <#if table.columnList??>
    <#list table.columnList as column>
    final ${util.dartType(column)} ${column.property};
    </#list>
  </#if>

  bool selected = false;
}