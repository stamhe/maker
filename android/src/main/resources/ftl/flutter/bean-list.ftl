import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../../../config.dart';
import '../../abstract-loading-list.dart';
import '${util.dartName(table.javaName)}.dart';

class ${util.firstUpper(table.javaName)}ListPage extends LoadingListPage<${util.firstUpper(table.javaName)}> {
  ${util.firstUpper(table.javaName)}ListPage({Key key, String title}) : super(key: key, title: title);

  bool _loading = false;
  int _page = 1;

  @override
  Future<List<${util.firstUpper(table.javaName)}>> loadData(bool isfresh) {
    return _load(isfresh);
  }


  Future<List<${util.firstUpper(table.javaName)}>> _load(bool isfresh) async {
    if (_loading) {
      return null;
    }
    _loading = true;
    if (isfresh) {
      _page = 1;
    }
    try {
      var url = "$SERER_URL/${util.firstLower(table.javaName)}/list/json";
      print("url: $url");
      var resp = await http.get(url);
      print("result: ${r'${'}resp.body}");
      var data = json.decode(resp.body)['data']['beans'];
      print("list: $data");
      _page += 1;
      List<${util.firstUpper(table.javaName)}> list = new List();
      if (data is List) {
        data.forEach((dynamic e) {
          ${util.firstUpper(table.javaName)} ${util.firstLower(table.javaName)} = new ${util.firstUpper(table.javaName)}(${util.dartJsonBeanParams(table, 'e')});
          list.add(${util.firstLower(table.javaName)});
        });
      }
      return Future.value(list);
    } finally {
      _loading = false;
    }
  }

  @override
  Widget buildListCell(BuildContext context, ${util.firstUpper(table.javaName)} ${util.firstLower(table.javaName)}) {
    return ListTile(
      key: new ValueKey<String>('${r'${'}${util.firstLower(table.javaName)}.${table.primaryKey.column.property}}'),
      title: new Text('${r'${'}${util.firstLower(table.javaName)}.${util.dartBeanName(table, 1)}??""}'),
      subtitle: new Text('${r'${'}${util.firstLower(table.javaName)}.${util.dartBeanName(table, 2)}??""}'),
      trailing: new Icon(Icons.star_half, size: 23.0, color: Colors.blueAccent),
    );
  }
}


