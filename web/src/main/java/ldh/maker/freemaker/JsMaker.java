package ldh.maker.freemaker;

import ldh.database.Column;
import ldh.database.Table;
import ldh.maker.util.FreeMakerUtil;

import java.util.ArrayList;
import java.util.List;

public class JsMaker extends FreeMarkerMaker<JsMaker> {
	
	private Table table;
	private String ftl;
	private List<Column> columns = new ArrayList<Column>();
	
	public JsMaker table(Table table) {
		this.table = table;
		for (Column c : table.getColumnList()) {
			if (FreeMakerUtil.isDate(c)) {
				Column cc = new Column(c.getName(), "开始" + c.getComment(), c.getType());
				cc.setProperty("start" + FreeMakerUtil.firstUpper(c.getProperty()));
				Column cc2 = new Column(c.getName(), "结束" + c.getComment(), c.getType());
				cc2.setProperty("end" + FreeMakerUtil.firstUpper(c.getProperty()));
				columns.add(cc);
				columns.add(cc2);
			} else {
				columns.add(c);
			}
			
		}
		return this;
	}
	
	public JsMaker ftl(String ftl) {
		this.ftl = ftl;
		return this;
	}
	
	public JsMaker make() {
		data();
		out(ftl, data);
		
		
		return this;
	}
	
	public void data() {
		check();
		data.put("table", table);
		data.put("columns", columns);
//		data.put("module", MakerConfig.getInstance().getParam().getModule());
	}
	
	
	protected void check() {
		if (table == null) {
			throw new NullPointerException("table must not be null");
		}
		if (ftl == null) {
			throw new NullPointerException("ftl must not be null");
		}
		fileName = FreeMakerUtil.firstLower(table.getJavaName()) + ".js";
		super.check();
		
	}
}
