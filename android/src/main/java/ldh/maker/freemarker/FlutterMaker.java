package ldh.maker.freemarker;

import ldh.maker.FlutterIcon;
import ldh.maker.database.TableInfo;
import ldh.maker.freemaker.FreeMarkerMaker;
import ldh.maker.freemaker.SimpleJavaMaker;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by ldh123 on 2018/10/27.
 */
public class FlutterMaker extends FreeMarkerMaker<FlutterMaker> {

    protected TableInfo tableInfo;

    public FlutterMaker tableInfo(TableInfo tableInfo) {
        this.tableInfo = tableInfo;
        return this;
    }

    public FlutterMaker data(String key, Object value) {
        data.put(key, value);
        return this;
    }

    @Override
    public FlutterMaker make() {
        data();
        this.out(ftl, data);
        return this;
    }

    @Override
    public void data() {
        if (tableInfo == null) return;
        data.put("tableInfo", tableInfo);
        List<String> icons = Arrays.stream(FlutterIcon.values()).map(flutterIcon -> flutterIcon.name()).collect(Collectors.toList());
        String iconName = icons.get(0);
        for (int i=icons.size(); i<tableInfo.getTables().size(); i++) {
            icons.add(iconName);
        }
        data.put("icons", icons);
    }
}
